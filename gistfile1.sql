UPDATE 
  `media_movies` 
SET 
  `media_movies`.`tmdb_id` = '86838' ,
  `media_movies`.`release` = '2012-10-12' ,
  `media_movies`.`average_vote` = '6.2' , 
  `media_movies`.`original_title` = 'Seven Psychopaths' ,
  `media_movies`.`poster_path` = 'http://image.tmdb.org/t/p/w500/3Zs6Ne6QAEOzQRz85VToH8ml6ab.jpg' 
WHERE 
  `media_movies`.`title` = '7 Psychos';